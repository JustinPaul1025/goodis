<?php

use App\Models\Test;
use App\Models\Label;
use Illuminate\Database\Seeder;

class TestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test = Test::Create([
            'name' => 'Drug Test',
            'description' => 'test description',
            'price' => '0',
            'status' => 1,
            'text_to_display' => 'edit here'
        ]);

        $label = Label::Create([
            'test_id' => 1, 
            'label' => 'result',
            'normal_value' => ''
        ]);

        $test = Test::Create([
            'name' => 'X-Ray',
            'description' => 'test description',
            'price' => '0',
            'status' => 1,
            'text_to_display' => 'edit here'
        ]);

        $label = Label::Create([
            'test_id' => 2, 
            'label' => 'result',
            'normal_value' => ''
        ]);

    }
}
