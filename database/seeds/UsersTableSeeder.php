<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $user = User::Create([
            'username' => 'admin',
            'password' => bcrypt('123123'),
            'first_name' => 'John',
            'middle_name' => 'John',
            'last_name' => 'Doe',
            'role' => 'admin'
        ]);

        $user = User::Create([
            'username' => 'cashier',
            'password' => bcrypt('123123'),
            'first_name' => 'John',
            'middle_name' => 'John',
            'last_name' => 'Doe',
            'role' => 'cashier'
        ]);

        $user = User::Create([
            'username' => 'medtech',
            'password' => bcrypt('123123'),
            'first_name' => 'John',
            'middle_name' => 'John',
            'last_name' => 'Doe',
            'role' => 'medtech'
        ]);
        
        $user = User::Create([
            'username' => 'client',
            'password' => bcrypt('123123'),
            'first_name' => 'Jane',
            'middle_name' => 'Jane',
            'last_name' => 'Doe',
            'role' => 'client'
        ]);
    }
}
