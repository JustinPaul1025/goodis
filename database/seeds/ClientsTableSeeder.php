<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Client::Create([
            'first_name' => 'Jane',
            'middle_name' => 'Jane',
            'last_name' => 'Doe',
            'birth_date' => '1998/01/01',
            'gender' => 1,
            'contact_no' => '09090909090',
            'address' => 'Lorem ipsum, Lorem City'
        ]);
    }
}
