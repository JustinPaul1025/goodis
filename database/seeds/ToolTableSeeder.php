<?php

use App\Models\Tool;
use Illuminate\Database\Seeder;

class ToolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tool = Tool::create([
            'current_number' => 0,
            'cashier_queue' => 1,
            'medtech_queue' => 1,
            'auto_reset' => 0,
            'data' => date("Y/m/d"),
            'auto_update' => 0,
            'ring' => 0,
            'current_cashier_queue' => 0
        ]);
    }
}
