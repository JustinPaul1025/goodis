<?php

use App\Models\Signatory;
use Illuminate\Database\Seeder;

class SignatoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 3) as $range) {
            Signatory::create([
                'name' => "Edit This",
                'job_description' => "doctor"
            ]);
        }
    }
}
