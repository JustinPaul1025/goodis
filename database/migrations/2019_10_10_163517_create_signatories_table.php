<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatoriesTable extends Migration
{
    public function up()
    {
        Schema::create('signatories', function (Blueprint $table): void
         {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('job_description');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('signatories');
    }
}
