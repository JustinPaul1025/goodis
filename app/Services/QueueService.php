<?php

namespace App\Services;

use App\Models\Tool;

class QueueService
{

    public function kioskUpdate()
    {
        $queue = Tool::find(1);
        $queue->update([
            'current_number' => $queue->current_number + 1
        ]);
    }

    public function cashierUpdate()
    {
        $queue = Tool::find(1);
        $queue->update([
            'cashier_queue' => $queue->cashier_queue + 1
        ]);
    }

    public function medTechUpdate()
    {
        $queue = Tool::find(1);
        $queue->update([
            'medtech_queue' => $queue->medtech_queue + 1
        ]);
    }

    public function resetQueue()
    {
        $queue = Tool::find(1);
        $queue->update([
            'current_number' => 0,
            'cashier_queue' => 1,
            'medtech_queue' => 1,
            'data' => date("Y/m/d"),
            'current_queue' => 0,
            'current_cashier_queue' => 0
        ]);
    }
}