<?php

namespace App\Services;

use App\Transaction;


class TransactionServices
{
    public function toDeleteTransactions()
    {
        return (Transaction::whereIn('progress', [1, 2])->get());
    }
}