<?php

namespace App\Services;

use App\Models\Test;
use App\Transaction;
use App\Models\Promo;
use App\Services\AvailedServices;

class AvailedServices
{
    public function calculateTest(array $tests): Array
    {
        $returnData = [0, false];

        foreach ($tests as $test) {
            $data = Test::find($test['id']);
            if($data->id == 1)
                $returnData[1] = true;
            $returnData[0] = $returnData[0] + $data->price;
        }

        return $returnData;
    }

    public function calculatePromo(array $promos): Array
    {
        $returnData = [0, false];

        foreach ($promos as $promo) {
            $data = Promo::find($promo['id']);
            $promoTests = Promo::find($promo['id'])->tests;

            foreach ($promoTests as $promoTest) {
                if($promoTest['id'] == 1) {
                    $returnData[1] = true;
                }
            }
            
            $returnData[0] = $returnData[0] + $data->price;
        }

        return $returnData;
    }

    public function saveTest(array $tests, $id)
    {
        $test_id = [];
        foreach ($tests as $test) {
            array_push($test_id,$test['id']);
        }

        $transaction = Transaction::find($id);
        $transaction->tests()->sync($test_id);
    }

    public function savePromo(array $promos, $id)
    {
        $promo_id = [];
        foreach ($promos as $promo) {
            array_push($promo_id,$promo['id']);
        }
        
        $transaction = Transaction::find($id);
        $transaction->promos()->sync($promo_id);
    }

    public function implodeTest(array $tests, array $promos): String
    {
        $availed = [];

        foreach ($promos as $promo) {
            array_push($availed,$promo['name'].'-[P '.$promo['price'].']'.'(Promo Package)');
        }

        foreach ($tests as $test) {
            array_push($availed,$test['name'].'-[P '.$test['price'].']');
        }

        $implodeAvailed = implode(", ",$availed);

        return $implodeAvailed;
    }
    
}