<?php

namespace App\Services;

use App\Models\Test;
use App\Transaction;
use App\Models\Avail;
use App\Models\Result;

class MedTechServices
{
    public function setLabel($avail_id, $test_id, $transaction_id)
    {
        $test = Test::find($test_id);
        $labels = $test->labels;
        foreach ($labels as $label) {
            Result::create([
                'avail_id' => $avail_id,
                'label' => $label['label'],
                'normal_value' => $label['normal_value']
            ]);
        }

        return response()->json($labels);
    }

    public function checkResults($id)
    {
        $isDone = true;
        $avail = Avail::find($id);
        foreach ($avail->results as $result) {
            if($result['result'] === null) {
                $isDone = false;
            }
        }

        return $isDone;
    }

    public function checkAvails($id)
    {
        $isDone = true;
        $transaction = Transaction::find($id);
        foreach ($transaction->avails as $avail) {
            if($avail['is_done'] === 0) {
                $isDone = false;
            }
        }

        return $isDone;
    }
}