<?php

namespace App\Models;

use App\Models\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Promo extends Model
{
    protected $fillable = [
        'name',
        'price',
        'status'
    ];

    public function tests(): BelongsToMany
    {
        return $this->belongsToMany(Test::class, 'promo_test');
    }

    public function transactions(): BelongsToMany
    {
        return $this->belongsToMany(Transacion::class, 'promo_transaction');
    }
}
