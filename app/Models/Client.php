<?php

namespace App\Models;


use App\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends Model
{
    protected $fillable = [
        'first_name', 
        'middle_name', 
        'last_name',
        'birth_date',
        'gender',
        'contact_no',
        'address',
        'is_archived'
    ];

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}

