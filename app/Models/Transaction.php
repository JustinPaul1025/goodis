<?php

namespace App;

use App\Models\Test;
use App\Models\Avail;
use App\Models\Promo;
use App\Models\Client;
use App\Models\Income;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Transaction extends Model
{
    protected $fillable = [
        'client_id',
        'total',
        'queue',
        'progress',
        'id_for_drugtest',
        'status',
        'is_archived',
        'id_type'
    ];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
    
    public function promos(): BelongsToMany
    {
        return $this->belongsToMany(Promo::class, 'promo_transaction');
    }

    public function tests(): BelongsToMany
    {
        return $this->belongsToMany(Test::class, 'test_transactions');
    }

    public function income(): HasOne
    {
        return $this->hasOne(Income::class);
    }

    public function avails(): HasMany
    {
        return $this->hasMany(Avail::class);
    }
}
