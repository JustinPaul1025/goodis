<?php

namespace App\Models;

use App\Models\Result;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Avail extends Model
{
    protected $fillable = [
        'transaction_id',
        'test_name',
        'is_done'
    ];

    public function results(): HasMany
    {
        return $this->hasMany(Result::class);
    }
}
