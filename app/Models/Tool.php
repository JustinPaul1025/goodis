<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $fillable = [
        'current_number',
        'cashier_queue',
        'medtech_queue',
        'auto_reset',
        'data',
        'auto_update',
        'ring',
        'current_queue',
        'current_cashier_queue'
    ];
}
