<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'avail_id',
        'label',
        'result',
        'normal_value'
    ];
}
