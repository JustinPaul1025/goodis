<?php

namespace App\Models;

use App\Transaction;
use App\Models\Label;
use App\Models\Promo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Test extends Model
{
    protected $fillable = [
        'name', 
        'description', 
        'price',
        'status',
        'text_to_display'
    ];

    public function labels(): HasMany
    {
        return $this->hasMany(Label::class);
    }

    public function promos(): BelongsToMany
    {
        return $this->belongsToMany(Promo::class, 'promo_test');
    }

    public function transactions(): BelongsToMany
    {
        return $this->belongsToMany(Transaction::class, 'test_transaction');
    }
}
