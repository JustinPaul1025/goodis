<?php

namespace App\Events;

use App\Models\Tool;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MedTechQueue implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $queue_no;
    protected $ring;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        $tool = Tool::find(1);
        $this->queue_no = $tool->medtech_queue;
        $this->ring = $tool->medtech_queue;
    }

    public function broadcastWith()
    {
        return array(
            'queue' => $this->queue_no,
            'ring' => $this->ring
        );
    }

    public function broadcastOn()
    {
        return new PrivateChannel('queueChannel');
    }
}
