<?php

namespace App\Http\Controllers\API;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function profile(): JsonResponse
    {
        return response()->json(auth('api')->user());
    }

    public function index(): JsonResponse
    {
        //dd(auth('api')->user()->id);
        $users = User::latest()->where('id', '!=', auth('api')->user()->id)->paginate(5);

        return response()->json($users);
    }
    
    public function store(Request $request): JsonResponse
    {
        $currentData = User::create([
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'role' => $request->input('role'),
        ]);

        return response()->json($currentData);
    }

    public function update(Request $request, User $user): JsonResponse
    {
        $user
          ->update([
                'username' => $request->input('username'),
                'first_name' => $request->input('first_name'),
                'middle_name' => $request->input('middle_name'),
                'last_name' => $request->input('last_name'),
                'role' => $request->input('role'),
            ]);
        
        return response()->json($user);
    }

    public function destroy(User $user): JsonResponse
    {
        return response()->json($user->delete());
    }

    public function search(Request $request, $keyword): JsonResponse
    {
        $response = User::latest()->where('first_name', 'like', "%{$keyword}%")
                    ->orWhere('last_name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }

    public function changePassword(Request $request, User $user)
    {
        $user
            ->update([
                'password' => Hash::make($request->input('password'))
            ]);

        return response()->json($user);
    }
}
