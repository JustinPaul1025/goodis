<?php

namespace App\Http\Controllers\API;

use App\Transaction;
use App\Models\Avail;
use Illuminate\Http\Request;
use App\Services\QueueService;
use App\Services\MedTechServices;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class MedTechController extends Controller
{
    public function testToPerform(Request $request, Transaction $transaction, QueueService $queueService, MedTechServices $medTechServices): JsonResponse
    {
        $done = 0;
        $inc = 0;
        
        foreach ($request->input('performTest') as $test) {
            $inc++;
            if($test['name'] === 'Drug Test') {
                $done = 1;
            }

            $avail = Avail::create([
                'transaction_id' => $transaction->id,
                'test_name' => $test['name'],
                'is_done' => $done,
            ]);

            $medTechServices->setLabel($avail->id, $test['id'], $transaction->id);
        };

        $transaction
            ->update([
                'progress' => $transaction->progress + 1,
            ]);
        
        if($transaction->is_archived){
            $transaction
                ->update([
                    'is_archived' => 0,
                ]);
        } else {
            $queueService->medTechUpdate();
        }

        if($done === 1 && $inc === 1) {
            $transaction
                ->update([
                    'progress' => 4,
                ]);
        }

        return response()->json('Success');

    }
}
