<?php

namespace App\Http\Controllers\API;

use App\Models\Label;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class LabelController extends Controller
{
    public function getLabels($id): JsonResponse
    {
        $labels = Label::where('test_id', $id)->get();

        return response()->json($labels);
    }


    public function store(Request $request)
    {
        $currentData = Label::create([
            'test_id' => $request->input('test_id'),
            'label' => $request->input('label'),
            'normal_value' => $request->input('normal_value'),
        ]);

        return response()->json($currentData);
    }

    public function update(Request $request, Label $label): JsonResponse
    {
        $label
            ->update([
                'label' => $request->input('label'),
                'normal_value' => $request->input('normal_value'),
            ]);

        return response()->json($label);
    }

    public function destroy(Label $label): JsonResponse
    {   
        return response()->json($label->delete());
    }
}
