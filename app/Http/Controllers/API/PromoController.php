<?php

namespace App\Http\Controllers\API;

use App\Models\Test;
use App\Models\Promo;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    public function index(): JsonResponse
    {
        $promo = Promo::paginate(4);

        return response()->json($promo);
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = Promo::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'), 
            'status' => 1
        ]);

        $promo = Promo::find($currentData->id);
        $test_id = [];

        foreach ($request->input('tests') as $test) {
            array_push($test_id,$test['id']);
        }

        $promo->tests()->sync($test_id);

        return response()->json($promo);
    }

    public function update(Request $request, Promo $promo): JsonResponse
    {
        $promo->update([
            'name' => $request->input('name'),
            'price' => $request->input('price'), 
            'status' => $request->input('status'), 
        ]);

        $test_id = [];

        foreach ($request->input('tests') as $test) {
            array_push($test_id,$test['id']);
        }

        $promo->tests()->sync($test_id);

        return response()->json($promo);
    }

    public function testForSelect(): JsonResponse
    {
        return response()->Json(Test::where('status', true)->get());
    }

    public function promoTest(Promo $promo): JsonResponse
    {
        return response()->json($promo->tests);
    }

    public function promoForSelect(): JsonResponse
    {
        return response()->json(Promo::where('status', true)->get());
    }
}
