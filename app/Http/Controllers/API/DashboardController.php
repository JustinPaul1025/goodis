<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Income;
use App\Transaction;
use Illuminate\Http\JsonResponse;
use Carbon\Carbon;

class DashboardController extends Controller
{
    
    public function getUserCount(): JsonResponse
    {
        $count = Client::count();
        
        return response()->json($count);
    }

    public function getTransactionCount(): JsonResponse
    {
        $count = Transaction::count();
        
        return response()->json($count);
    }

    public function getTotalSales(): JsonResponse
    {   
        $sum = Income::sum('amount_paid');
        
        return response()->json($sum);
    }

    public function getWeeklySales(): JsonResponse
    {
        $weekly = Income::whereBetween('created_at', [
            Carbon::parse('last monday')->startOfDay(),
            Carbon::parse('next monday')->endOfDay(),
        ])->sum('amount_paid');

        return response()->json($weekly);
    }

    public function getMonthlySales(): JsonResponse
    {
        $monthly = Income::whereMonth('created_at',date('m'))->sum('amount_paid');

        return response()->json($monthly); 
    }

    public function getYearlySales(): JsonResponse
    {
        $yealy = Income::whereYear('created_at',date('Y'))->sum('amount_paid');

        return response()->json($yealy); 
    }

    public function getOneWeekSales(): JsonResponse
    {
        // $date = \Carbon\Carbon::today()->subDays(30);
        // $users = Income::whereDate('created_at', Carbon::now()->subDays(7))
        // ->get()->groupBy('created_at');

        $weekly = Income::whereBetween('created_at', [
            Carbon::parse('last monday')->startOfDay(),
            Carbon::parse('next sunday')->endOfDay(),
        ])->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('D');
        })->map(function ($row) {
            return $row->sum('amount_paid');
        });
        
        return response()->json($weekly);
    }

    // public function getOneMonthSales(): JsonResponse
    // {
    //     $fromDate = Carbon::now()->startOfMonth()->toDateString();
    //     $tillDate = Carbon::now()->endOfMonth()->toDateString();

    //     $weekly = Income::whereBetween('created_at', [$fromDate, $tillDate])->get()->groupBy(function($date) {
    //         return Carbon::parse($date->created_at)->format('W');
    //     })->map(function ($row) {
    //         return $row->sum('amount_paid');
    //     });


        
    //     return response()->json($weekly);
    // }

    public function getEveryMonth(): JsonResponse
    {
        $weekly = Income::all()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('M');
        })->map(function ($row) {
            return $row->sum('amount_paid');
        });
        
        return response()->json($weekly);
    }

}
