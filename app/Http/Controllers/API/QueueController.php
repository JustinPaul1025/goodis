<?php

namespace App\Http\Controllers\API;

use App\Models\Tool;
use App\Transaction;
use Illuminate\Http\Request;
use App\Services\QueueService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Services\TransactionServices;

class QueueController extends Controller
{
    public function queueSet($id)
    {
        $queue = Tool::find(1);
        $queue->update([
            'auto_reset' => $id
        ]);

        return response()->json($queue->auto_reset);
    }

    public function manualReset(TransactionServices $transactionServices, QueueService $queueService)
    {
        $queueService->resetQueue();
        $transactions = $transactionServices->toDeleteTransactions();
        foreach ($transactions as $transaction) {
            Transaction::where('id', $transaction['id'])->forceDelete();
        }

        return response()->json($queueService);
    }

    public function resetQueue(TransactionServices $transactionServices, QueueService $queueService)
    {
        $queue = Tool::find(1);

        if($queue->auto_reset) {
            if($queue->data !== date("Y-m-d")) {
                $queueService->resetQueue();
                $transactions = $transactionServices->toDeleteTransactions();
                foreach ($transactions as $transaction) {
                    Transaction::where('id', $transaction['id'])->forceDelete();
                }
            }
        }

        return response()->json($queue);
    }

    public function getQueue(Tool $tool): JsonResponse
    {
        $data = $tool;

        if($tool->ring) {
            $tool
             ->update([
                'ring' => 0
             ]);

            $data->ring = 1;
        }

        return response()->json($data);
    }

    public function ringBell(Tool $tool): JsonResponse
    {
        $tool
            ->update([
               'ring' => 1
            ]);

        return response()->json('Ringed');
    }

    public function incCashierQueue(Tool $tool): JsonResponse
    {
        $tool
            ->update([
                'cashier_queue' => $tool->cashier_queue++
            ]);
        
        return response()->json('incremented');
    }

    public function startMedtech()
    {
        $queue = Tool::find(1);
        $queue->update([
            'medtech_queue' => 1
        ]);

        return response()->json($queue);
    }

    public function incMedtech()
    {
        $queue = Tool::find(1);
        $queue->update([
            'medtech_queue' => $queue->medtech_queue  + 1,
        ]);

        return response()->json($queue);
    }

    public function print(Transaction $transaction)
    {
        return view('queue_print', compact('transaction'));
    }

    public function incCurrentNumber($number)
    {
        $tool = Tool::find(1);
        $tool->update([
            'current_queue' => $number
        ]);

        return response()->json($tool);
    }

    public function getSetting(Tool $tool)
    {
        return response()->json($tool->auto_reset);
    }
}
