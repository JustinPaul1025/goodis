<?php

namespace App\Http\Controllers\API;

use App\Models\Signatory;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class SignatoryController extends Controller
{

    public function index(): JsonResponse
    {
        $sig = Signatory::latest()->get();

        return response()->json($sig);
    }

    public function update(Request $request, Signatory $signatory): JsonResponse
    {
        $signatory->update([
                'name' => $request->input('name'),
                'job_description' => $request->input('job_description'),
            ]);
        
        return response()->json($signatory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Signatory  $signatory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Signatory $signatory)
    {
        //
    }
}
