<?php

namespace App\Http\Controllers\API;

use App\Transaction;
use App\Models\Client;
use App\Models\Income;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Avail;

class ClientController extends Controller
{
    public function index(): JsonResponse
    {
        $clients = Client::where('is_archived', 0)->orWhereNull('is_archived')->paginate(5);
        
        return response()->json($clients);
    }

    public function revertArchive(Client $client): JsonResponse
    {
        $client->update([
            'is_archived' => 0
        ]);

        return response()->json($client); 
    }   

    public function archives(): JsonResponse
    {
        $clients = Client::where('is_archived', 1)->get();

        return response()->json($clients);
    }
    
    public function store(Request $request): JsonResponse
    {
        $currentData = Client::create([
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'birth_date' => $request->input('birth_date'),
            'gender' => $request->input('gender'),
            'contact_no' => $request->input('contact_no'),
            'address' => $request->input('address'),
        ]);

        return response()->json($currentData);
    }

    public function show(Client $client): JsonResponse
    {
        
        return response()->json($client);
    }

    public function update(Request $request, Client $client): JsonResponse
    {
        $client
          ->update([
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'birth_date' => $request->input('birth_date'),
            'gender' => $request->input('gender'),
            'contact_no' => $request->input('contact_no'),
            'address' => $request->input('address'),
            ]);
        
        return response()->json($client);
    }

    public function destroy(Client $client): JsonResponse
    {
        return response()->json($client->update(['is_archived' => true]));
    }

    public function search(Request $request, $keyword): JsonResponse
    {
        $response = Client::where('first_name', 'like', "%{$keyword}%")
                    ->orWhere('last_name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }

    public function getTransactions(Client $client)
    {
        $data = $client->transactions;
        $toQuery = array();
        foreach ($data as $data) {
            array_push($toQuery,$data['id']);
        }
        $transactions = Income::whereIn('transaction_id', $toQuery)->get();

        return response()->json($transactions);
    }

    public function getResults(Client $client)
    {
        $data = $client->transactions;
        $toQuery = array();
        foreach ($data as $data) {
            array_push($toQuery,$data['id']);
        }
        $avail = Avail::whereIn('transaction_id', $toQuery)->where('is_done', 1)->get();

        return response()->json($avail);
    }

    public function clientTransaction(Request $request)
    {
        $client = Client::where('first_name', $request->input('first_name'))
                    ->where('middle_name', $request->input('middle_name'))
                    ->where('last_name', $request->input('last_name'))
                    ->get();

        if(!$client->isEmpty()) {
            return response()->json($client[0]);
        } else {
            $currentData = Client::create([
                'first_name' => $request->input('first_name'),
                'middle_name' => $request->input('middle_name'),
                'last_name' => $request->input('last_name'),
                'birth_date' => $request->input('birth_date'),
                'gender' => $request->input('gender'),
                'contact_no' => $request->input('contact_no'),
                'address' => $request->input('address'),
            ]);
    
            return response()->json($currentData);
        }
    }
}
