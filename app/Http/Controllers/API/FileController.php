<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\File;

class FileController extends Controller
{

    public function index(): JsonResponse
    {
        $files = File::latest()->get();

        return response()->json($files);
    }
    

    public function store(Request $request): JsonResponse
    {
        $fileName = $request->input('name').'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('upload'), $fileName);

        $currentData = File::create([
            'name' => $request->input('name'),
            'extension' => $request->file->getClientOriginalExtension(),
            'is_playing' => 0,
        ]);
              
        return response()->json($currentData);
    }

    public function destroy($id): JsonResponse
    {
        $video = File::findOrFail($id);
        $path = public_path('upload')."/".$video->name.".".$video->extension;
        unlink($path);
        
        $video->delete();

        return response()->json($video);
    }

    public function update($id): JsonResponse
    {

        File::where('is_playing', 1)
            ->update([
                'is_playing' => '0'
            ]);

        $is_playing = File::where('id', $id)
            ->update([
                'is_playing' => '1'
            ]);
        
        return response()->json($is_playing);
    }

    public function getVideo($id): JsonResponse
    {
        $playing = File::where('is_playing', $id)->first();

        return response()->json($playing);
    }
}
