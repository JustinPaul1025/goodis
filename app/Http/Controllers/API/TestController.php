<?php

namespace App\Http\Controllers\API;

use App\Models\Test;
use App\Models\Label;
use App\Models\Avail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index(): JsonResponse
    {
        $tests = Test::latest()->where('status', true)
        ->paginate(10);

        return response()->json($tests);
    }

    public function store(Request $request): JsonResponse
    {
        $test= Test::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'status' => '1',
            'text_to_display' => $request->input('text_to_display'),
        ]);

        foreach ($request->input('labels') as $label) {
            Label::create([
                'test_id' => $test->id,
                'label' => $label['label'],
                'normal_value' => $label['normal_value']
            ]);
        }

        return response()->json("Insert Successfully");
    }

    public function show(Test $test): JsonResponse
    {
        return response()->json($test);
    }

    public function update(Request $request, Test $test): JsonResponse
    {
        $test
          ->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
                'text_to_display' => $request->input('text_to_display'),
            ]);
        
        return response()->json($test);
    }

    public function archive($id): JsonResponse
    {
        $response = Test::where('id', $id)
            ->update([
                'status' => '0'
            ]);
            
        return response()->json($response);
    }

    public function viewArchived(): JsonResponse
    {
        return response()->json($tests = Test::where('status', false)->get());
    }

    public function activateTest($id): JsonResponse 
    {
        $response = Test::where('id', $id)
            ->update([
                'status' => '1'
            ]);
            
        return response()->json($response);
    }

    public function availableTest(): JsonResponse
    {
        $tests = Test::where('status', true)->get();

        return response()->json($tests);
    }

    public function testForSelect(): JsonResponse
    {
        return response()->json($tests = Test::where('status', true)->get());
    }

    public function getTestReport()
    {
        
        $data = Avail::latest()->get()->groupBy(function($item) {
            return $item->test_name;
        })->map(function ($row, $key) {
            return [$key, $row->count('transaction_id')];
        });

        return response()->json($data);
    }

    public function getTestMonthlyReport($date)
    {

        $monthly = Avail::latest()->whereMonth('created_at', Carbon::parse($date)->format('m'))->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('F');
        })->map(function ($row, $key) {
            return [$key, $row->groupBy(function ($val) {
                return $val->test_name;
            })->map(function ($row, $key) {
                return [$key, $row->count('transaction_id')];
            })];
        });

        return response()->json($monthly);
    }

    public function getTestDailyReport($date)
    {

        $monthly = Avail::latest()->whereDate('created_at', Carbon::parse($date)->format('Y-m-d'))->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('F d, ');
        })->map(function ($row, $key) {
            return [$key, $row->groupBy(function ($val) {
                return $val->test_name;
            })->map(function ($row, $key) {
                return [$key, $row->count('transaction_id')];
            })];
        });

        return response()->json($monthly);
    }

    public function search(Request $request, $keyword): JsonResponse
    {
        $response = Avail::latest()->where('test_name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }

    public function print($date, $id)
    {
        $datas;
        $title;

        if($id === "empty") {

            $datas = Avail::latest()->get()->groupBy(function($item) {
                return $item->test_name;
            })->map(function ($row, $key) {
                return [$key, $row->count('transaction_id')];
            });

            $title = "All";
        }

        if($id === "daily") {

            $datas =Avail::latest()->whereDate('created_at', Carbon::parse($date)->format('Y-m-d'))->get()->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('F d, ');
            })->map(function ($row, $key) {
                return [$key, $row->groupBy(function ($val) {
                    return $val->test_name;
                })->map(function ($row, $key) {
                    return [$key, $row->count('transaction_id')];
                })];
            });

            $title = "Daily";
        }

        if($id === "month") {

            $datas = Avail::latest()->whereMonth('created_at', Carbon::parse($date)->format('m'))->get()->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('F');
            })->map(function ($row, $key) {
                return [$key, $row->groupBy(function ($val) {
                    return $val->test_name;
                })->map(function ($row, $key) {
                    return [$key, $row->count('transaction_id')];
                })];
            });

            $title = "Month";
        }

        return view('print_test_reports', compact('datas', 'title', 'date'));
    }
}
