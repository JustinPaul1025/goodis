<?php

namespace App\Http\Controllers\API;

use App\Transaction;
use App\Models\Client;
use App\Models\Income;
use App\Models\Tool;
use Illuminate\Http\Request;
use App\Services\AvailedServices;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class CashierController extends Controller
{

    public function getTotal(Request $request, AvailedServices $availedService): JsonResponse
    {
        $total = 0;

        $result = $availedService->calculateTest($request->input('selectedTest'));
        $total = $total + $result[0];

        $result = $availedService->calculatePromo($request->input('selectedPromo'));
        $total = $total + $result[0];
        
        return response()->json($total);
    }

    public function show(Transaction $transaction)
    {
        return response()->json($transaction);
    }

    public function getNextClient()
    {
        $transaction = Transaction::where('progress', 1)->where('is_archived', null)->orderBy('queue', 'asc')->take(1)->get();
        
        $tool = Tool::find(1);
        
        $tool->update([
            'current_cashier_queue' => $transaction[0]->queue
        ]);

        return response()->json($transaction);
    }

    public function getArchived()
    {
        return response()->json(Transaction::where('progress', 1)->where('is_archived', 1)->with('client')->get());
    }

    public function archiveTransaction(Transaction $transaction)
    {
        $transaction->update([
            'is_archived' => 1
        ]);

        return response()->json($transaction);
    }

    public function getChange($total, $amount): JsonResponse
    {
        $change = 0;

        $change = $amount - $total;

        return response()->json($change);
    }

    public function printReciept(Income $income)
    {
        $transaction = Transaction::find($income->transaction_id);
        $tests = $transaction->tests;
        $promos = $transaction->promos;
        $client = $transaction->client;

        return view('cashier_reciept', compact('income', 'transaction', 'tests', 'promos', 'client'));
    }
    
}
