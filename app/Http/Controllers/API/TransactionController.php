<?php

namespace App\Http\Controllers\API;

use App\Models\Tool;
use App\Transaction;
use App\Models\Income;
use Illuminate\Http\Request;
use App\Services\QueueService;
use App\Services\AvailedServices;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use PDF;

class TransactionController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(Transaction::latest()->with('client')->get());
    }

    public function cashierTransaction(): JsonResponse
    {
        return response()->json(Transaction::with('client')->whereIn('progress', [1])->get());
    }

    public function currentTransactionMedTech($queue): JsonResponse
    {
        $data = Transaction::where('queue', $queue)->whereIn('progress', [1,2])->first();
        return response()->json($data);
    }

    public function store(Request $request, AvailedServices $availedService, QueueService $queueService): JsonResponse
    {
        $queue = Tool::find(1);
        $total = 0;

        $result = $availedService->calculateTest($request->input('selectedTest'));
        $total = $total + $result[0];

        $result = $availedService->calculatePromo($request->input('selectedPromo'));
        $total = $total + $result[0];

        $id_type;

        if($request['is_government']) {
            $id_type = $request['id_type'];
        } else {
            $id_type = 'Others';
        }

        $data = Transaction::create([
            'client_id' => $request['client_id'],
            'total' => $total,
            'queue' => $queue->current_number+1,
            'progress' => 1,
            'id_for_drugtest' => $request['id_for_drugtest'],
            'id_type' => $id_type
        ]);

        $availedService->saveTest($request->input('selectedTest'), $data->id);

        $availedService->savePromo($request->input('selectedPromo'), $data->id);
        
        $queueService->kioskUpdate();

        return response()->Json($data);
    }

    public function show(Transaction $transaction): JsonResponse
    {

        $transaction = Transaction::where("queue", $id)->get();

        return response()->json($transaction);

    }

    public function currentTransaction($queue): JsonResponse
    {
        $match = ['queue' => $queue, 'progress' => 1];
        return response()->json(Transaction::where($match)->first());
    }

    public function currentCashierTransaction($transaction)
    {
        return response()->json(Transaction::with('client')->find($transaction));
    }

    public function update(Request $request, Transaction $transaction, AvailedServices $availedService, QueueService $queueService)
    {
        $total = 0;

        $result = $availedService->calculateTest($request->input('selectedTest'));
        $total = $total + $result[0];

        $result = $availedService->calculatePromo($request->input('selectedPromo'));
        $total = $total + $result[0];

        $transaction
            ->update([
                'total' => $total,
                'progress' => $transaction->progress + 1,
            ]);

        $availedService->saveTest($request->input('selectedTest'), $transaction->id);

        $availedService->savePromo($request->input('selectedPromo'), $transaction->id);

        $queueService->cashierUpdate();

        $availed = $availedService->implodeTest($request->input('selectedTest'), $request->input('selectedPromo'));

        $income = Income::create([
            'transaction_id' => $transaction->id,
            'availed' => $availed,
            'amount_paid' => $total
        ]);

        // return response()->json('success');
        $tests = [];
        foreach($request->input('selectedTest') as $selected) {
            $tests[] = [
                'name' => $selected['name'],
                'price' => $selected['price'],
                'description' => $selected['description']
            ];
        }

        $promos = [];
        foreach($request->input('selectedPromo') as $selected) {
            $promos[] = [
                'name' => $selected['name'],
                'price' => $selected['price'],
            ];
        }

        $queue = Tool::find(1);
        if($transaction->queue >= $queue->medtech_queue ) {
            $transaction
            ->update([
                'is_archived' => 0
            ]);
        } else {
            $transaction
            ->update([
                'is_archived' => 1
            ]);
        }

        return response()->json($income);
    }

    public function destroy($id)
    {
        //
    }

    public function getTests(Transaction $transaction): JsonResponse
    {
        return response()->json($transaction->tests);
    }

    public function getPromos(Transaction $transaction): JsonResponse
    {
        return response()->json($transaction->promos);
    }

    public function pendingTransaction()
    {
        return response()->json(Transaction::whereIn('progress', [1,2])->orderBy('queue', 'asc')->take(6)->get());
    }

    public function search(Request $request, $keyword): JsonResponse
    {
        $response = Transaction::latest()->with('client')->whereHas('client', function ($query) use ($keyword) {
            $query->where('first_name', 'like', "%{$keyword}%")
                ->orWhere('last_name', 'like', "%{$keyword}%");
        })
        ->orWhere('queue', $keyword)->get();

        return response()->json($response);
    }

    public function getArchived()
    {
        return response()->json(Transaction::where('progress', 2)
                                ->where('is_archived', 1)
                                ->with('client')
                                ->orderBy('queue', 'asc')
                                ->get());        
    }

    public function showArchivedTransaction(Transaction $transaction)
    {
        return response()->json($transaction);
    }
}
