<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Avail;
use App\Models\Client;
use App\Models\Result;
use App\Models\Signatory;
use App\Services\MedTechServices;
use App\Transaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class XrayController extends Controller
{
    public function save(Request $request, Avail $avail, MedTechServices $medTechServices): JsonResponse
    { 

    	$testResult;
        if($request->result === null) {
            $testResult = '   ';
        } else {
            $testResult = $request->result;
        }

        $result = Result::find($request->id);
        $result
            ->update([
            	'label' => $request->label,
                'result' => $testResult
            ]);

        $isDone = $medTechServices->checkResults($avail->id);

        if($isDone) {
            $avail
                ->update([
                    'is_done' => true
                ]);
        };

        $isDone = $medTechServices->checkAvails($avail->transaction_id);
        if($isDone) {
            Transaction::find($avail->transaction_id)
                ->update([
                    'progress' => 4
                ]);
        }

        return response()->json($avail);
    }

    public function print(Avail $avail, $name, $position)
    {
    	$result = $avail->results;
    	$signatory = Signatory::latest()->first();
        $transaction = Transaction::find($avail->transaction_id);
        $client = Client::find($transaction->client_id);

        return view('print_lab_result_xray', compact('result', 'name', 'position', 'transaction', 'client', 'avail', 'signatory'));
    }
}
