<?php

namespace App\Http\Controllers\API;

use App\Models\Test;
use App\Models\Promo;
use Illuminate\Http\Request;
use App\Services\AvailedServices;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Services\GetTotalAvailedService;

class AvailController extends Controller
{
    public function checkTest(Request $request, AvailedServices $availedService): JsonResponse
    {
        $returnData = [0, false];
        
        $result = $availedService->calculateTest($request->input('selectedTest'));
        $returnData[0] = $returnData[0] + $result[0];
        $returnData[1] = $result[1];

        $result = $availedService->calculatePromo($request->input('selectedPromo'));
        $returnData[0] = $returnData[0] + $result[0];
        if($returnData[1] != true) {
            $returnData[1] = $result[1];
        }

        return response()->json($returnData);
    }
}
