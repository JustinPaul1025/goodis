<?php

namespace App\Http\Controllers\API;

use App\Models\Income;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IncomeController extends Controller
{
    
    public function index(): JsonResponse
    {
        $data = Income::latest()->with('transaction', 'transaction.client')->paginate(10);

        return response()->json($data);
    }

    public function search(Request $request, $keyword): JsonResponse
    {
        $response = Income::with('transaction', 'transaction.client')->get();

        // $response->client()->fr
        //             // ->where('first_name', 'like', "%{$keyword}%")
        //             // ->orWhere('last_name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }

    // public function getDailyIncomeReportSearch($date): JsonResponse
    // {

    //     $daily = Income::whereDate('created_at', $date)->get()->groupBy(function($item) {
    //         return [$item->created_at->format('Y-m-d')];
    //     })->map(function ($row, $key) {
    //         return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
    //     });

    //     return response()->json($daily);
    // }

    // public function getDailyIncomeReport(): JsonResponse
    // {

    //     $daily = Income::latest('created_at')->get()->groupBy(function($item) {
    //         return [$item->created_at->format('Y-m-d')];
    //     })->map(function ($row, $key) {
    //         return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
    //     });

    //     return response()->json($daily);
    // }

    // public function getWeeklyIncomeReport(): JsonResponse
    // {

    //     $weekly = Income::latest()->get()->groupBy(function($item) {
    //         return Carbon::parse($item->created_at)->format('W');
    //     })->map(function ($row, $key) {
    //         return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
    //     });

    //     return response()->json($weekly);
    // }

    // public function getMonthlyIncomeReport(): JsonResponse
    // {

    //     $monthly = Income::latest()->get()->groupBy(function($item) {
    //         return Carbon::parse($item->created_at)->format('M');
    //     })->map(function ($row, $key) {
    //         return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
    //     });

    //     return response()->json($monthly);
    // }

    // public function getYearlyIncomeReport(): JsonResponse
    // {

    //     $yearly = Income::latest()->get()->groupBy(function($item) {
    //         return Carbon::parse($item->created_at)->format('Y');
    //     })->map(function ($row, $key) {
    //         return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
    //     });

    //     return response()->json($yearly);
    // }

    public function getDailyReport($date): JsonResponse
    {

        $daily = Income::latest()->with('transaction', 'transaction.client')->whereDate('created_at', Carbon::parse($date)->format('Y-m-d'))->get();

        return response()->json($daily);
    }

    public function getModifiedReport($from, $to): JsonResponse
    {
        $fromm    = Carbon::parse($from)
                 ->startOfDay()        // 2018-09-29 00:00:00.000000
                 ->toDateTimeString(); // 2018-09-29 00:00:00

        $too = Carbon::parse($to)
                 ->endOfDay()          // 2018-09-29 23:59:59.000000
                 ->toDateTimeString();

        $mod = Income::latest()->with('transaction', 'transaction.client')->whereBetween('created_at', [Carbon::parse($fromm)->format('Y-m-d H:i:s'), Carbon::parse($too)->format('Y-m-d H:i:s')])->get();
        return response()->json($mod);
    }

    public function getMonthlyReport($date): JsonResponse
    {

        $monthly = Income::latest()->with('transaction', 'transaction.client')->whereMonth('created_at', Carbon::parse($date)->format('m'))->get();

        return response()->json($monthly);
    }

    public function getYearlyReport($date): JsonResponse
    {

        $yearly = Income::latest()->with('transaction', 'transaction.client')->whereYear('created_at', $date)->get();

        return response()->json($yearly);
    }

    public function print($date, $id)
    {
        $datas;
        $title;
        $datas2;

        if($id === "empty") {
            $datas2 = Income::latest('created_at')->get()->groupBy(function($item) {
                        return [$item->created_at->format('Y-m-d')];
                    })->map(function ($row, $key) {
                        return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
                    });
                    
            $datas = Income::latest()->with('transaction', 'transaction.client')->get();

            $title = "All";
        }

        if($id === "daily") {

            $datas2 = Income::latest('created_at')->get()->groupBy(function($item) {
                        return [$item->created_at->format('Y-m-d')];
                    })->map(function ($row, $key) {
                        return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
                    });

            $datas = Income::latest()->with('transaction', 'transaction.client')->whereDate('created_at', Carbon::parse($date)->format('Y-m-d'))->get();

            $title = "Daily";
            $date = Carbon::parse($date)->format('F d, Y');
        }

        if($id === "month") {

            $datas2 = Income::latest()->get()->groupBy(function($item) {
                        return Carbon::parse($item->created_at)->format('M');
                    })->map(function ($row, $key) {
                        return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
                    });

            $datas = Income::latest()->with('transaction', 'transaction.client')->whereMonth('created_at', Carbon::parse($date)->format('m'))->get();

            $title = "Month";
            $date = Carbon::parse($date)->format('F Y');
        }

        if($id === "year") {

            $datas2 = Income::latest()->with('transaction', 'transaction.client')->get()->groupBy(function($item) {
                    return Carbon::parse($item->created_at)->format('M');
                })->map(function ($row, $key) {
                    return [$key, $row->sum('amount_paid'), $row->count('transaction_id'), $row->count('client')];
                });

            $datas = Income::latest()->with('transaction', 'transaction.client')->whereYear('created_at', $date)->get();

            $title = "Year";
        }

        $modi = null;
        
        $dataCount = $datas->count();

        return view('print_income_reports', compact('datas', 'dataCount', 'datas2', 'title', 'date', 'modi'));
    }

    public function printModified($from, $to, $id)
    {
        $datas;
        $title;
        $datas2;
        $modi;

        $datas2 = Income::latest('created_at')->get()->groupBy(function($item) {
            return [$item->created_at->format('Y-m-d')];
        })->map(function ($row, $key) {
            return [$key, $row->sum('amount_paid'), $row->count('transaction_id')];
        });
        $fromm    = Carbon::parse($from)
        ->startOfDay()        // 2018-09-29 00:00:00.000000
        ->toDateTimeString(); // 2018-09-29 00:00:00

        $too = Carbon::parse($to)
                ->endOfDay()          // 2018-09-29 23:59:59.000000
                ->toDateTimeString();

        $datas = Income::latest()->with('transaction', 'transaction.client')->whereBetween('created_at', [Carbon::parse($fromm)->format('Y-m-d H:i:s'), Carbon::parse($too)->format('Y-m-d H:i:s')])->get();
        $dataCount = $datas->count();

        $title = "Custom";
        $from = Carbon::parse($fromm)->format('F d, Y');
        $to = Carbon::parse($too)->format('F d, Y');
        $modi ="modified";
        $date = $from . " - " . $to;

        return view('print_income_reports', compact('datas', 'title', 'datas2', 'from', 'to', 'modi', 'dataCount', 'date'));
    }
}
