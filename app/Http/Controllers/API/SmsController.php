<?php

namespace App\Http\Controllers\API;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Twilio\Rest\Client as Clients;
use Twilio\Jwt\ClientToken;
use Twilio\Exceptions\TwilioException;
use Log;

class SmsController extends Controller
{
    public function sendSms(Request $request, Client $client): JsonResponse
    {
        $name = $client->where('id', $request->input('id'))->get();

        $accountSid = 'ACe0b2b437efcd76bd36150c77dc0d9959';
        $authToken = '6aa3ba0c5c65db5e3dce3fa8c3783a14';
        $twilioNumber = "+12564826346";

        $number = $name[0]->contact_no;
        $number = ltrim($number,"0");

        $to = '+63'.$number;
            $user = $name[0]->first_name.' '.$name[0]->middle_name.' '.$name[0]->last_name;

        $client = new Clients($accountSid, $authToken);

        try {
            $client->messages->create(
                $to,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+12564826346',
                    // the body of the text message you'd like to send
                    'body' => 'Hi '.$user.', Your result has been successfully processed! Please claim it immediately.'
                )
            );
            Log::info('Message sent to ' . $twilioNumber);
        } catch (Exception $e) {
            Log::error(
                'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e
            );
        }

        return response()->json($client);
    }
}
