<?php

namespace App\Http\Controllers\API;

use PDF;
use App\Transaction;
use App\Models\Avail;
use App\Models\Client;
use App\Models\Result;
use App\Models\Signatory;
use Illuminate\Http\Request;
use App\Services\MedTechServices;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ResultController extends Controller
{
    public function index(): JsonResponse
    {
        $transaction = Transaction::latest()->where('progress', '>=', 3)->with('client')->paginate(10);

        return response()->json($transaction);
    }

    public function getAvails(Transaction $transaction): JsonResponse
    {
        return response()->json($transaction->avails);
    }

    public function getResults(Avail $avail): JsonResponse
    {
        return response()->json($avail->results);
    }

    public function saveResult(Request $request, Avail $avail, MedTechServices $medTechServices): JsonResponse
    { 

        foreach ($request->input('results') as $updateResult){
            $testResult;
            if($updateResult['result'] === null) {
                $testResult = '   ';
            } else {
                $testResult = $updateResult['result'];
            }

            $result = Result::find($updateResult['id']);
            $result
                ->update([
                    'result' => $testResult
                ]);
        };

        $isDone = $medTechServices->checkResults($avail->id);

        if($isDone) {
            $avail
                ->update([
                    'is_done' => true
                ]);
        };

        $isDone = $medTechServices->checkAvails($avail->transaction_id);
        if($isDone) {
            Transaction::find($avail->transaction_id)
                ->update([
                    'progress' => 4
                ]);
        }

        return response()->json($avail);
    }

    public function claim(Transaction $transaction): JsonResponse
    {
        if($transaction->status === 'claimed') {
            $transaction
                ->update([
                    'status' => ''
                ]);
        } else {
            $transaction
                ->update([
                    'status' => 'claimed'
                ]); 
        }
        
        return response()->json($transaction);
    }

    public function print(Avail $avail)
    {
        $results = $avail->results;
        $signatories = Signatory::latest()->get();
        $transaction = Transaction::find($avail->transaction_id);
        $client = Client::find($transaction->client_id);

        return view('print_lab_result', compact('results', 'signatories', 'transaction', 'client', 'avail'));
    }
}
