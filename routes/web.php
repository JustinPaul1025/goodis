<?php

use App\Http\Controllers\API\XrayController;
use App\Http\Controllers\QueueController;

Route::get('/queue', [QueueController::class, 'index']);

Auth::routes();
Route::get('{path}','HomeController@index')->where( 'path', '([A-z\d/_.]+)?' );

Route::post('printResultXray/{avail}', [XrayController::class, 'print']);


