<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['user' => 'API\UserController']);
Route::get('user/search/{keyword}', 'API\UserController@search');
Route::get('getCurrentUser', 'API\UserController@profile');
Route::put('changePassword/{user}', 'API\UserController@changePassword');

Route::apiResources(['client' => 'API\ClientController']);
Route::get('client/search/{keyword}', 'API\ClientController@search');
Route::get('client/transactions/{client}', 'API\ClientController@getTransactions');
Route::get('client/results/{client}', 'API\ClientController@getResults');
Route::get('client/archives/view', 'API\ClientController@archives');
Route::post('client/clientTransaction', 'API\ClientController@clientTransaction');

Route::apiResources(['test' => 'API\TestController']);
Route::put('test/archive/{id}', 'API\TestController@archive');
Route::put('test/activate/{id}', 'API\TestController@activateTest');
Route::get('testForSelect', 'API\TestController@testForSelect');
Route::get('viewArchived', 'API\TestController@viewArchived');
Route::get('loadAvailableTest', 'API\TestController@availableTest');
Route::put('client/revertArchive/{client}', 'API\ClientController@revertArchive');
Route::get('getTestReport', 'API\TestController@getTestReport');
Route::get('getTestMonthlyReport/{date}', 'API\TestController@getTestMonthlyReport');
Route::get('getTestDailyReport/{date}', 'API\TestController@getTestDailyReport');
Route::get('test/getTestSearch/{keyword}', 'API\TestController@search');
Route::get('printTestReport/{date}/{id}', 'API\TestController@print');

Route::apiResources(['label' => 'API\LabelController']);
Route::get('getTestLabels/{id}', 'API\LabelController@getLabels');

Route::apiResources(['promo' => 'API\PromoController']);
Route::get('getAvailableTest', 'API\PromoController@testForSelect');
Route::get('promoTest/{promo}', 'API\PromoController@promoTest');
Route::get('promoForSelect', 'API\PromoController@promoForSelect');

Route::post('checkTest', 'API\AvailController@checkTest');

Route::apiResources(['transaction' => 'API\TransactionController']);
Route::get('getPending', 'API\TransactionController@pendingTransaction');
Route::get('cashierTransaction', 'API\TransactionController@cashierTransaction');
Route::get('currentTransaction/{queue}', 'API\TransactionController@currentTransaction');
Route::get('cashierCurrentTransaction/{transaction}', 'API\TransactionController@currentCashierTransaction');
Route::get('currentTransactionMedTech/{queue}', 'API\TransactionController@currentTransactionMedTech');
Route::get('archivedTransaction/{transaction}', 'API\TransactionController@showArchivedTransaction');
Route::get('transactionTest/{transaction}', 'API\TransactionController@getTests');
Route::get('transactionPromo/{transaction}', 'API\TransactionController@getPromos');
Route::get('generateCashierPdf', 'API\TransactionController@getPDF');
Route::get('transaction/search/{keyword}', 'API\TransactionController@search');

Route::apiResources(['cashier' => 'API\CashierController']);
Route::post('cashier/total' , 'API\CashierController@getTotal');
Route::get('cashier/change/{total}/{amount}', 'API\CashierController@getChange');
Route::get('print-reciept/{income}', 'API\CashierController@printReciept');
Route::get('nextClient', 'API\CashierController@getNextClient');
Route::get('getArchivedClient', 'API\CashierController@getArchived');
Route::get('archivedClient/{transaction}', 'API\CashierController@archiveTransaction');
Route::get('clientDetail/{transaction}', 'API\CashierController@show');
Route::get('medTechArchive', 'API\TransactionController@getArchived');

Route::put('testToPerform/{transaction}', 'API\MedTechController@testToPerform');

Route::get('getQueue/{tool}', 'API\QueueController@getQueue');
Route::put('incrementCurrentNumber/{number}', 'API\QueueController@incCurrentNumber');
Route::get('incCashierQueue/{tool}', 'API\QueueController@incCashier');
Route::get('startMedtechQueue', 'API\QueueController@startMedtech');
Route::get('incMedtechQueue', 'API\QueueController@incMedtech');
Route::get('resetQueue', 'API\QueueController@resetQueue');
Route::get('getQueueSetting/{tool}', 'API\QueueController@getSetting');
Route::get('queueSet/{id}', 'API\QueueController@queueSet');
Route::get('manualReset', 'API\QueueController@manualReset');

Route::apiResources(['videos' => 'API\FileController']);
Route::get('videos/getVideo/{playing}', 'API\FileController@getVideo');

Route::apiResources(['signatories' => 'API\SignatoryController']);

Route::get('ringBell/{tool}', 'API\QueueController@ringBell');

Route::get('getResults', 'API\ResultController@index');
Route::get('getTestAvailed/{transaction}', 'API\ResultController@getTestAvailed');
Route::get('getAvailedResults/{avail}', 'API\ResultController@getResults');
Route::get('getAvails/{transaction}', 'API\ResultController@getAvails');
Route::put('saveResult/{avail}', 'API\ResultController@saveResult');
Route::put('saveResultXray/{avail}', 'API\XrayController@save');
Route::get('claim/{transaction}', 'API\ResultController@claim');
Route::get('printResult/{avail}', 'API\ResultController@print');
Route::get('printResultXray/{avail}/{name}/{position}', 'API\XrayController@print');

Route::get('income', 'API\IncomeController@index');
Route::get('printIncome/{date}/{id}', 'API\IncomeController@print');
Route::get('printModifiedIncome/{from}/{to}/{id}', 'API\IncomeController@printModified');
Route::get('printQueue/{transaction}', 'API\QueueController@print');
Route::get('income/search/{keyword}', 'API\IncomeController@search');
Route::get('getDailyReport/{date}', 'API\IncomeController@getDailyReport');
Route::get('getModifiedReport/{from}/{to}', 'API\IncomeController@getModifiedReport');
Route::get('getMonthlyReport/{date}', 'API\IncomeController@getMonthlyReport');
Route::get('getYearlyReport/{date}', 'API\IncomeController@getYearlyReport');
Route::get('getDailyIncomeReportSearch/{date}', 'API\IncomeController@getDailyIncomeReportSearch');

Route::get('getUserCount', 'API\DashboardController@getUserCount');
Route::get('getTotalSales', 'API\DashboardController@getTotalSales');
Route::get('getWeeklySales', 'API\DashboardController@getWeeklySales');
Route::get('getMonthlySales', 'API\DashboardController@getMonthlySales');
Route::get('getYearlySales', 'API\DashboardController@getYearlySales');
Route::get('getOneWeekSales', 'API\DashboardController@getOneWeekSales');
Route::get('getEveryMonth', 'API\DashboardController@getEveryMonth');
Route::get('getTransactionCount', 'API\DashboardController@getTransactionCount');

Route::post('sendSms', 'API\SmsController@sendSms');