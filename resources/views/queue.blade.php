<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body>
    
<div id="app">
    <nav class="navbar topqueue" role="navigation">
        <div class="navbar-brand">
            <a class="p-t-15 p-b-10 p-l-10" href="#">
                <p class="title has-text-white">Good Samaritan Diagnostic Hub Inc.</p>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-end">
                <div class="navbar-item">
                <a class="navbar-link has-font-weight-bold has-text-white is-arrowless has-background-primary">
                    <p class="is-size-5">12:00 AM DD/MM/YYYY</p>
                </a>
                <a href="/" class="icon has-text-white"><i class="fa fa-sign-out-alt"></i></a>
                </div>
            </div>
        </div>
    </nav>
    <queue-component></queue-component>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/main.js') }}" defer></script>

</body>
</html>
