<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body class="login-background">
    <section class="hero is-fullheight is-bold">
        <div class="hero-body">
            <div class="container" id="login-box">
            <div class="columns is-vcentered is-centered">
                <div class="column is-6 is-paddingless right-content ">
                <div class="box is-marginless header-box has-text-centered">
                    <p>Good Samaritan Diagnostic Hub Inc.</p>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="box has-background-secondary form-box">
                        <label class="label has-text-white is-size-5">Username</label>
                        <p class="control has-icon has-icon-right">
                        <input id="username" type="text" class="input @error('email') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="username">

                        @error('username')
                            <span class="invalid-feedback has-text-white" role="alert">
                                <strong class="has-text-danger">{{ $message }}</strong>
                            </span>
                        @enderror
                        <span class="icon is-small">
                            <i class="fa fa-warning"></i>
                        </span>
                        </p>
                        <label class="label has-text-white is-size-5">Password</label>
                        <p class="control has-icon has-icon-right">
                        <input type="password" placeholder="●●●●●●●" class="input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong class="has-text-danger">{{ $message }}</strong>
                            </span>
                        @enderror
                        <span class="icon is-small">
                            <i class="fa fa-warning"></i>
                        </span>
                        </p>
                        <hr>
                        <p class="control is-pulled-right">
                        <a href="{{ route('queue') }}" class="button button-secondary has-text-weight-bold has-text-primary">Open Queue Window</a>
                        <button type="submit" class="button is-primary has-text-weight-bold">
                            {{ __('Login') }}
                        </button>
                        </p>
                        <br><br>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    </section>
</body>
</html>
