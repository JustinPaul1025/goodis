<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<style>
.test-result-title {
    font-weight: bold;
    color: #147a30;
}

.bold-font {
  font-weight: bold;
  font-size: 100px;
}
</style>
<body style="padding: 30px 20px;font-family: 'Nunito', sans-serif;" onload="printData()">

  <button onclick="printData()" id="hidebutton">Print me</button>
    <h3 class="title is-3 test-result-title p-t-10" style="padding-top: 10px !important;font-family: 'Nunito', sans-serif;">GOOD SAMARITAN DIAGNOSTIC HUB</h3>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Panabo, Doctors Bldg, Quezon St, New Pandan, Panabo City</p>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Tel (084) 628-6743/09277799312</p>
  <br>
  <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">BILL TO:</p>
<p class="is-size-5" style="font-family: 'Nunito', sans-serif;"><b>{{$client->last_name}}, {{$client->first_name}}</b></p>
  <br>
  
  <table class="table" style="border: 1px solid #ddd; width: 100%;border-collapse: collapse;
    border-spacing: 0;color: black;">
    <thead>
      <tr>
        <th style="width: 400px;text-align: center;background-color: #147a2f6e;">Description</th>
        <th style="text-align: center;background-color: #147a2f6e;">Type</th>
        <th style="text-align: center;background-color: #147a2f6e;">Amount</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tests as $test)
        <tr>
          <td style="text-align: center;border: 1px solid #ddd;padding-left: 15px;">{{$test->name}}</td>
          <td style="text-align: center;border: 1px solid #ddd;">Single Test</td>
          <td style="text-align: right;border: 1px solid #ddd;">P {{$test->price}}</td>
        </tr>
      @endforeach
      @foreach($promos as $promo)
        <tr>
          <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{$promo->name}}</td>
          <td style="text-align: center;border: 1px solid #ddd;">Promo Package</td>
          <td style="text-align: right;border: 1px solid #ddd;">P {{$promo->price}}</td>
        </tr>
      @endforeach
      <tr>
        <td style="text-align: left;"></td>
        <td style="text-align: right;">Total:</td>
        <td style="text-align: right;">P {{$income->amount_paid}}</td>
      </tr>
    </tbody>
  </table>
  <br>
</body>
</html>
<script type="text/javascript">
	function printData()
	{
		document.getElementById("hidebutton").hidden = false;
	  print();
	  document.getElementById("hidebutton").hidden = true;
	}

	$('button').on('click',function(){
	  printData();
	})
</script>