@extends('layouts.app')

@section('content')
    <div class="box is-marginless content-header has-text-right">
        <p>Content Header</p>
    </div>
    <div class="box is-marginless content-body has-text-centered">
        <p>Good Samaritan Diagnostic Hub Inc.</p>
    </div>
    
    @can('isPatient')
        <client-component></client-component>
    @endcan
@endsection
