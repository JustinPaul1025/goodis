<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<style>
.test-result-title {
    font-weight: bold;
    color: #147a30;
}
</style>
<body onload="printData()">
    <div id="app">
        <div class="container has-text-centered m-t-10" id="printTable">
            <h3 style="font-size: 20px;font-weight: 800;
    color: #147a30;line-height: 1.125;word-break: break-word;">GOOD SAMARITAN DIAGNOSTIC HUB</h3>
            <p class="subtitle test-result-subtext" style="margin-bottom: 5px;">Panabo, Doctors Bldg, Quezon St, New Pandan, Panabo City</p>
            <p class="subtitle test-result-subtext" style="margin-bottom: -15px;">Tel (084) 628-6743/09277799312</p>
            <br>
            <p class="subtitle is-size-6 has-text-centered has-text-weight-bold test-result-subtext m-b-0">{{ $avail->test_name }}</p>
            <div class="has-text-left" style="width: 100%; margin-left: 120px; margin-right: auto;">
                <table class="table no-border">
                    <tr>
                        <td style="border: none;vertical-align: bottom;width: 60px; padding-bottom: 0px;margin-bottom: 0px" class=" test-result-subtext">
                            Name
                        </td>
                        <td style="border: none;vertical-align: bottom;padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px"  class="p-b-0 subtitle test-result-subtext">
                            :
                        </td>
                        <td style=" border-bottom: 1px solid #f11e1e; padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px; width: 40%; font-size: 14px" class="has-text-black has-text-weight-bold">
                            {{ $client->last_name }}, {{ $client->first_name }} {{ $client->middle_name }}
                        </td>
                        <td style="border: none;width: 10px;">

                        </td>
                        <td style="border: none;vertical-align: bottom; ; padding-bottom: 0px;margin-bottom: 0px" class="test-result-subtext">
                            Age/Sex
                        </td>
                        <td style="border: none;vertical-align: bottom;padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px"  class="p-b-0 subtitle test-result-subtext">
                            :
                        </td>
                        <td style=" border-bottom: 1px solid #f11e1e; padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px; font-size: 14px" class="has-text-black has-text-weight-bold">
                            {{ $client->gender ? "Male" : "Female" }}
                        </td>
                    
                    </tr>
                    <tr>
                        <td style="border: none;vertical-align: bottom;width: 60px;padding-bottom: 0px;margin-bottom: 0px" class=" test-result-subtext">
                            Address
                        </td>
                        <td style="border: none;vertical-align: bottom;padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px"  class="p-b-0 subtitle test-result-subtext">
                            :
                        </td>
                        <td style=" border-bottom: 1px solid #f11e1e; padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px; width: 40%; font-size: 14px" class="has-text-black has-text-weight-bold">
                            {{ $client->address }}
                        </td>
                        <td style="border: none;width: 100px;">

                        </td>
                        <td style="border: none;vertical-align: bottom;width: 60px;padding-bottom: 0px;margin-bottom: 0px" class=" test-result-subtext">
                            Date
                        </td>
                        <td style="border: none;vertical-align: bottom;padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px"  class="p-b-0 subtitle test-result-subtext">
                            :
                        </td>
                        <td style=" border-bottom: 1px solid #f11e1e; padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px; font-size: 14px" class="has-text-black has-text-weight-bold">
                            @php
                                {{echo date("d/m/Y");}}
                            @endphp
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;vertical-align: bottom;width: 60px;padding-bottom: 0px;margin-bottom: 0px" class=" test-result-subtext">
                            Case#
                        </td>
                        <td style="border: none;vertical-align: bottom;padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px"  class="p-b-0 subtitle test-result-subtext">
                            :
                        </td>
                        <td style=" border-bottom: 1px solid #f11e1e; padding-bottom: 0px; margin-left: 0px; margin-bottom: 0px; width: 40%; font-size: 14px" class="has-text-black has-text-weight-bold">
                            {{ $transaction->id }}
                        </td>
                        <td style="border: none;vertical-align: bottom;" class="p-b-0 subtitle test-result-subtext is-size-6">
                        </td>
                        <td style="border: none;vertical-align: bottom;"  class="p-b-0 subtitle test-result-subtext">
                        </td>
                        <td style="border: none;">
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="has-text-left" style="width: 80%; margin-left: 100px; justify-content:center;">
                <vue-html data-html="{{$result[0]->result}}"></vue-html>
            </div>
            <br>
            <div class="has-text-left" style="width: 80%; margin-left: 90px;">
            <table class="table no-border" style="margin-left: 30px">
                    <tr>
                        <td style="border-bottom: 1px solid #f11e1e;vertical-align: bottom;width: 285px;font-size: 12px" class="has-text-centered p-b-0">
                            <b>{{$signatory->name}}</b>
                        </td>
                        <td style="border: none;">

                        </td>
                        <td style="border-bottom: 1px solid #f11e1e;vertical-align: bottom;width: 285px;font-size: 12px" class="has-text-centered p-b-0">
                            <b>{{$name}}</b>
                        </td>
                        <td style="border: none;">

                        </td>
                    </tr>
                    <tr>
                        <td style="border: none;vertical-align: top;width: 80px; font-size: 12px" class="has-text-centered p-t-0 test-result-subtext">
                            <b>{{$signatory->job_description}}</b>
                        </td>
                        <td style="border: none;">

                        </td>
                        <td style="border: none;vertical-align: top;width: 80px; font-size: 12px" class="has-text-centered p-t-0 test-result-subtext">
                            {{$position}}
                        </td>
                        <td style="border: none;">

                        </td>
                    </tr>
                </table>
            </div>
    </tbody>
  </table>
        </div>
    </div>
<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
<script type="text/javascript">

	function printData()
	{
        print();
	}

	$('button').on('click',function(){
	    printData();
	})
</script>