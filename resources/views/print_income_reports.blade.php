<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<style>
.test-result-title {
    font-weight: bold;
    color: #147a30;
}
</style>
<body style="padding: 30px 20px;font-family: 'Nunito', sans-serif;">
  <div id="printTable" style="text-align: center">
    <h3 class="title is-3 test-result-title p-t-10" style="padding-top: 10px !important;font-family: 'Nunito', sans-serif;">GOOD SAMARITAN DIAGNOSTIC HUB</h3>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Panabo, Doctors Bldg, Quezon St, New Pandan, Panabo City</p>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Tel (084) 628-6743/09277799312</p>
    <br>
  <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">INCOME REPORT</p>
  @if($modi == null)
    <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">{{ $date }}</p>
  @else
    <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">From: &nbsp;{{ $from }} &nbsp;&nbsp;To: &nbsp;{{ $to }}</p>
  @endif
    <br>
    <table class="table" style="border: 1px solid #ddd; width: 100%;border-collapse: collapse;
      border-spacing: 0;color: black;">
      @if( $title == 'Year')
      <thead>
        <tr>
          <th style="width: 200px;text-align: center;background-color: #147a2f6e;">Month</th>
          <th style="text-align: center;background-color: #147a2f6e;">Total No. of Clients</th>
          <th style="text-align: center;background-color: #147a2f6e;">Total Income</th>
        </tr>
      </thead>
      @else
      <thead>
        <tr>
          <th style="width: 190px;text-align: center;background-color: #147a2f6e;">{{ $title == 'Daily' ? 'Time' : 'Date & Time' }}</th>
          <th style="text-align: center;background-color: #147a2f6e;">Transaction_id</th>
          <th style="text-align: center;background-color: #147a2f6e;">Client</th>
          <th style="text-align: center;background-color: #147a2f6e;">Availed</th>
          <th style="text-align: center;background-color: #147a2f6e;">Amount Paid</th>
        </tr>
      </thead>
      @endif
      <tbody>
        @if( $title == 'All' || $title == 'Daily')
            @php $i = 0;@endphp
          @foreach($datas as $data)
            <tr>
              <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ Carbon\Carbon::parse($data->created_at)->format('h:i:s a') }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction_id }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction->client->last_name }}, {{ $data->transaction->client->first_name }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->availed }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $data->amount_paid }}</td>
            </tr>
            @foreach($datas2 as $dataa)
            @if(date('d-m-Y', strtotime($data->created_at)) === date('d-m-Y', strtotime($dataa[0])))
              @php $i++;@endphp
              @if($i == $dataa[2])
              <tr>
                <td colspan="2" style="text-align: right;border: 1px solid #ddd;"><b>Total Number of Clients:</b></td>
                <td colspan="1" style="text-align: right;border: 1px solid #ddd;">{{ $dataCount }}</td>
                <td colspan="1" style="text-align: right;border: 1px solid #ddd;"><b>Total:</b></td>
                <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $dataa[1] }}</td>
              </tr>
              @php $i = 0;@endphp
              @endif
            @else

            @endif
            @endforeach
          @endforeach

        @elseif( $title == 'Month' )
            @php $i = 0;@endphp
          @foreach($datas as $data)
            <tr>
              <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data->created_at }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction_id }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction->client->last_name }}, {{ $data->transaction->client->first_name }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->availed }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $data->amount_paid }}</td>
            </tr>
            @foreach($datas2 as $dataa)
            @if(date('m', strtotime($data->created_at)) === date('m', strtotime($dataa[0])))
              @php $i++;@endphp
              @if($i == $dataa[2])
              <tr>
                <td colspan="2" style="text-align: right;border: 1px solid #ddd;"><b>Total Clients:</b></td>
                <td style="text-align: center;border: 1px solid #ddd;">{{ $dataCount }}</td>
                <td colspan="1" style="text-align: right;border: 1px solid #ddd;"><b>Total:</b></td>
                <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $dataa[1] }}</td>
              </tr>
              @php $i = 0;@endphp
              @endif
            @else

            @endif
            @endforeach
          @endforeach
        @elseif( $title == 'Year')
            @php $i = 0;@endphp
          @foreach($datas as $data)
            <!-- <tr>
              <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data->created_at }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction_id }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction->client->last_name }}, {{ $data->transaction->client->first_name }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->availed }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $data->amount_paid }}</td>
            </tr> -->
            @foreach($datas2 as $dataa)
            @if(date('m', strtotime($data->created_at)) === date('m', strtotime($dataa[0])))
              @php $i++;@endphp
              @if($i == $dataa[2])
              <tr>
                <td style="text-align: center;border: 1px solid #ddd;"> {{ date('F', strtotime($data->created_at)) }} {{ $date }} </td>
                <td style="text-align: center;border: 1px solid #ddd;"> {{ $dataa[3] }} </td>
                <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $dataa[1] }}</td>
              </tr>
              @php $i = 0;@endphp
              @endif
            @else

            @endif
            @endforeach
          @endforeach
        @else
        @php $total = 0;@endphp
          @foreach($datas as $data)
            <tr>
              <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data->created_at }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction_id }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->transaction->client->last_name }}, {{ $data->transaction->client->first_name }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data->availed }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $data->amount_paid }}</td>
              <div style="display: none">{{$total += $data->amount_paid}}</div>
            </tr>
          @endforeach
            <tr>
              <td colspan="2" style="text-align: right;border: 1px solid #ddd;"><b>No of Client:</b></td>
              <td style="text-align: center;border: 1px solid #ddd;"> {{ $dataCount }}</td>
              <td colspan="1" style="text-align: right;border: 1px solid #ddd;"><b>Total:</b></td>
              <td style="text-align: center;border: 1px solid #ddd;">&#8369; {{ $total }}</td>
            </tr>
        @endif
      </tbody>
    </table>
  </div>
  <br>
  <button onclick="printData()" id="hidebutton" class="">Print me</button>
  <p style="position: fixed; bottom: 0;" >Printed By: <b>Admin</b></p>
</body>
</html>
<script type="text/javascript">
	function printData()
	{
		document.getElementById("hidebutton").hidden = true;
	   print();
	   document.getElementById("hidebutton").hidden = false;
	}

	$('button').on('click',function(){
	printData();
	})
</script>