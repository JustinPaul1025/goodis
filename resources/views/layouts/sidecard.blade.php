<nav class="panel">
    <p class="panel-heading">
      <i class="fas fa-user-circle is-size-6 has-text-white "></i>&nbsp {{ Auth::user()->last_name }}, {{ Auth::user()->first_name }}
    </p>
    @can('isAdmin')
    {{-- <router-link to="/" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-chart-line"></i>
      </span>
      Dashboard
    </router-link> --}}
    <router-link to="/client" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-users"></i>
      </span>
      Clients
    </router-link>
    <router-link to="/cashier" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cash-register"></i>
      </span>
      Cashier
    </router-link>
    <router-link to="/medtech" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-microscope"></i>
      </span>
      Med-Tech
    </router-link>
    <router-link to="/medtech/archived" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-archive"></i>
      </span>
      Unsettled Transactions
    </router-link>
    <router-link to="/result" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-list-ol"></i>
      </span>
      Transactions
    </router-link>
    <router-link to="/report/select" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-copy"></i>
      </span>
      Reports
    </router-link>
    <router-link to="/test-result" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-clipboard"></i>
      </span>
      Results
    </router-link>
    <router-link to="/utility" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-sliders-h"></i>
      </span>
      Utilities
    </router-link>
    <router-link to="/user" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-user-md"></i>
      </span>
      Users
    </router-link>
    <router-link to="/settings" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cogs"></i>
      </span>
      User Settings
    </router-link>
    @endcan
    @can('isCashier')
    {{-- <router-link to="/" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-chart-line"></i>
      </span>
      Dashboard
    </router-link> --}}
    <router-link to="/cashier" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cash-register"></i>
      </span>
      Cashier
    </router-link>
    <router-link to="/settings" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cogs"></i>
      </span>
      User Settings
    </router-link>
    @endcan
    @can('isMedtech')
    {{-- <router-link to="/" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-chart-line"></i>
      </span>
      Dashboard
    </router-link> --}}
    <router-link to="/medtech" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-microscope"></i>
      </span>
      Med-Tech
    </router-link>
    <router-link to="/medtech/archived" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-archive"></i>
      </span>
      Unsettled Transactions
    </router-link>
    <router-link to="/test-result" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-clipboard"></i>
      </span>
      Results
    </router-link>
    <router-link to="/settings" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cogs"></i>
      </span>
      User Settings
    </router-link>
    @endcan
  </nav>