<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body class="main-background">
    <section class="hero is-fullheight is-bold">
        <div id="app">
            <div>
                @include('layouts.topnav') 
            </div>
            <div class="columns m-r-10 m-l-10">
            @canany(['isAdmin', 'isCashier', 'isMedtech'])
                <div class="column is-3">
                    <div id="sidebar">
                        @include('layouts.sidecard')
                    </div>
                </div>     
            @endcanany
                <div class="column">
                    <div id="main-content">
                        <router-view></router-view>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/main.js') }}" defer></script>
</body>
</html>
