<nav class="topnav navbar" role="navigation">
    <div class="navbar-brand">
        <a class="p-t-15 p-b-10" href="{{ url('/home') }}">
            <img src="{{ asset('img/goodis-navlogo.png') }}" width="112" height="15">
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu has-background-primary">
        <div class="navbar-end">
            <div class="navbar-item">
                <a class="navbar-item is-size-4 has-background-primary has-text-danger p-t-5" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                  <span><i class="fas is-size-4 fa-sign-out-alt has-text-danger"></i></span> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</nav>