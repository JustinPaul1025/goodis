<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<style>
.test-result-title {
    font-weight: bold;
    color: #147a30;
}

.bold-font {
  font-weight: bold;
  font-size: 100px;
}
</style>
<body style="padding: 30px 20px;font-family: 'Nunito', sans-serif;" onload="printData()">

  <div id="printTable" style="text-align: center">
    <h3 class="title is-3 test-result-title p-t-10" style="padding-top: 10px !important;font-family: 'Nunito', sans-serif;">GOOD SAMARITAN DIAGNOSTIC UB</h3>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Panabo, Doctors Bldg, Quezon St, New Pandan, Panabo City</p>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Tel (084) 628-6743/09277799312</p>
    <br><br>
    <code>Please keep this reciept responsibly</code>
    <br>
    <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">Medtech Window</p>
    <div>
      <h1 class="bold-font">NO.{{$transaction->queue}}</h1>
    </div>
    <button onclick="printData()" id="hidebutton">Print me</button>
  </div>
  <br>
</body>
</html>
<script type="text/javascript">
	function printData()
	{
		document.getElementById("hidebutton").hidden = false;
	  print();
	  document.getElementById("hidebutton").hidden = true;
	}

	$('button').on('click',function(){
	  printData();
	})
</script>