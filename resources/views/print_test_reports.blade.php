<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<style>
.test-result-title {
    font-weight: bold;
    color: #147a30;
}
</style>
<body style="padding: 30px 20px;font-family: 'Nunito', sans-serif;">
  <div id="printTable" style="text-align: center">
    <h3 class="title is-3 test-result-title p-t-10" style="padding-top: 10px !important;font-family: 'Nunito', sans-serif;">GOOD SAMARITAN DIAGNOSTIC HUB</h3>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Panabo, Doctors Bldg, Quezon St, New Pandan, Panabo City</p>
    <p class="subtitle m-b-10" style="font-family: 'Nunito', sans-serif;">Tel (084) 628-6743/09277799312</p>
    <br>
  <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">TEST REPORT</p>
  <p class="subtitle is-size-5 has-text-weight-bold m-b-10" style="color: black; font-weight: bold;font-family: 'Nunito', sans-serif;">{{ $title == 'Month' ? date('F Y', strtotime($date)) : date('F d, Y', strtotime($date)) }}</p>
    <br>
    @if( $title == 'All')
    <table class="table" style="border: 1px solid #ddd; width: 100%;border-collapse: collapse;
      border-spacing: 0;color: black;">
      <thead>
        <tr>
          <th style="text-align: center;background-color: #147a2f6e;">Test Name</th>
          <th style="text-align: center;background-color: #147a2f6e;">No. of Clients</th>
        </tr>
      </thead>
      <tbody>
          @foreach($datas as $data)
            <tr>
              <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data[0] }}</td>
              <td style="text-align: center;border: 1px solid #ddd;">{{ $data[1] }}</td>
            </tr>
            @endforeach
      </tbody>
    </table>
    @elseif( $title == 'Month' ||  $title == 'Daily')
    <table class="table" style="border: 1px solid #ddd; width: 100%;border-collapse: collapse;
      border-spacing: 0;color: black;">
      <thead>
        <tr>
        <th style="text-align: center;background-color: #147a2f6e;">Month</th>
          <th style="text-align: center;background-color: #147a2f6e;">Test Name</th>
          <th style="text-align: center;background-color: #147a2f6e;">No. of Clients</th>
        </tr>
      </thead>
      <tbody>
          @foreach($datas as $data)
            <tr>
              <td style="text-align: left;border-bottom: none;border-right: 1px solid #ddd;padding-left: 15px;">{{ $data[0] }} {{ date('Y', strtotime($date)) }}</td>
              <td rowspan="" style="text-align: left;border-bottom: none;padding-left: 15px;"></td>
              <td rowspan="" style="text-align: left;border-bottom: none;padding-left: 15px;"></td>
              @foreach($data[1] as $data)
              <tr>
                <td rowspan="" style="text-align: left;border-bottom: none;padding-left: 15px;"></td>
                <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data[0] }}</td>
                <td style="text-align: left;border: 1px solid #ddd;padding-left: 15px;">{{ $data[1] }}</td>
              </tr> 
              @endforeach
            </tr>
          @endforeach
      </tbody>
    </table>
    @endif
  </div>
  <br>
  <button onclick="printData()" id="hidebutton" class="">Print me</button>
  <p style="position: fixed; bottom: 0;" >Printed By: <b>Admin</b></p>
</body>
</html>
<script type="text/javascript">
	function printData()
	{
		document.getElementById("hidebutton").hidden = true;
	   print();
	   document.getElementById("hidebutton").hidden = false;
	}

	$('button').on('click',function(){
	printData();
	})
</script>