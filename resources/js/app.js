

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'

import VuejsPaginate from 'vuejs-paginate'
Vue.component('paginate', VuejsPaginate)

import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import SweetModal from 'sweet-modal-vue/src/plugin.js'
Vue.use(SweetModal)

Vue.use(require('vue-shortkey'))

import Swal from 'sweetalert2'
window.Swal = Swal;

import VueNumeric from 'vue-numeric'
Vue.use(VueNumeric)

import VueEditor from "vue2-editor/dist/vue2-editor.common.js";
Vue.use(VueEditor);

const routes = [

    { path: '/', component: require('./components/welcomeMessage.vue').default },
    { path: '/dashboard', component: require('./components/dashboard.vue').default },

    { path: '/cashier', component: require('./components/cashier/index.vue').default },
    { path: '/cashier/:id', component: require('./components/cashier/index.vue').default },

    { path: '/client', component: require('./components/client/index.vue').default },
    { path: '/client/transactions/:id', component: require('./components/client/transactions.vue').default },
    { path: '/client/:id', component: require('./components/cashier/index.vue').default },

    { path: '/medtech', component: require('./components/medtech/index.vue').default },
    { path: '/medtech/archived', component: require('./components/medtech/archived.vue').default },

    { path: '/report/select', component: require('./components/report/SelectReport.vue').default },
    { path: '/report', component: require('./components/report/index.vue').default },
    { path: '/report/incomes', component: require('./components/report/Income.vue').default },
    { path: '/report/test', component: require('./components/report/Test.vue').default },

    { path: '/result', component: require('./components/Transaction/index.vue').default },

    { path: '/test-result', component: require('./components/TestResult/Result/Index.vue').default },
    { path: '/test-result/receive', component: require('./components/TestResult/Unclaimed/Index.vue').default },
    { path: '/test-result/result', component: require('./components/TestResult/Result/Index.vue').default },
    { path: '/test-result/print', component: require('./components/TestResult/Result/Print.vue').default },
    { path: '/test/:id', component: require('./components/cashier/index.vue').default },
    
    { path: '/utility', component: require('./components/utility/index.vue').default },
    { path: '/utility/promo', component: require('./components/utility/promo/index.vue').default },
    { path: '/utility/test', component: require('./components/utility/test/index.vue').default },
    { path: '/utility/video', component: require('./components/utility/videos/index.vue').default },
    { path: '/utility/signatories', component: require('./components/utility/signatories/index.vue').default },

    { path: '/patient', component: require('./components/patient/Index.vue').default },
    { path: '/newpatient', component: require('./components/patient/NewPatient.vue').default },
    { path: '/viewpatient', component: require('./components/patient/ViewPatient.vue').default },
    { path: '/select/:id', component: require('./components/selectTest/index.vue').default },
    
    { path: '/user', component: require('./components/user/index.vue').default },

    { path: '/settings', component: require('./components/setting/index.vue').default },

  ]
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('queue-component', require('./components/Queue/Index.vue').default);
Vue.component('client-component', require('./components/patient/Index.vue').default);
Vue.component('vue-html', require('./components/VueHtml.vue').default);

const router = new VueRouter({
    mode: 'history',
    routes 
})

const app = new Vue({
    router
}).$mount('#app')
